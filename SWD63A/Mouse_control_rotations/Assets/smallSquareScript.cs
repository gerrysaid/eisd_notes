﻿using UnityEngine;
using System.Collections;

public class smallSquareScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

	void OnBecameInvisible()
	{
		//destroy all child objects
		foreach (Transform child in this.GetComponentsInChildren<Transform>()) {
			Destroy (child.gameObject);
		}
		Destroy (this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {


		//translate in LOCAL coordinates
		transform.Translate (new Vector3 (0f, 1f) * 5f * Time.deltaTime,Space.Self);
		//adding space.world translates in GLOBAL coordinates
		//transform.Translate (new Vector3 (0f, 1f) * 5f * Time.deltaTime,Space.World);


	
	}
}
