﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class turretRotation : MonoBehaviour {


	GameObject target;

	Transform turretTipLocation;

	GameObject bullet;

	public List<Vector3> trail;

	bool triangleIsMoving = false;

	public GameObject b;

	// Use this for initialization
	void Start () {
		trail = new List<Vector3> ();
		target = GameObject.Find ("MouseCursor");
		turretTipLocation = GameObject.Find ("TurretTip").GetComponent<Transform>();
		//loads the prefab from the file
		//instead of creating a public variable, I am loading the game object from the 
		//Resources/SmallSquareParent object
		bullet = Resources.Load ("SmallSquareParent") as GameObject;
	
	}

	IEnumerator moveTriangle(){
		
		int currentWaypoint = 0;
		int totalWaypoints = trail.Count;


		while (currentWaypoint < totalWaypoints) {
			while (Vector3.Distance (transform.position, trail [currentWaypoint]) > 0.5f) {
				triangleIsMoving = true;
				Vector3 difference = trail [currentWaypoint] - this.transform.position;

				float rotationInZ = Mathf.Atan2 (difference.y, difference.x) * Mathf.Rad2Deg;

				transform.rotation = Quaternion.Euler (new Vector3 (0f, 0f, rotationInZ));

				transform.position = Vector3.MoveTowards (transform.position, trail [currentWaypoint], 0.5f);

				yield return new WaitForSeconds (0.5f);
			}
			currentWaypoint++;
			yield return null;
		}
		triangleIsMoving = false;
		yield return null;
	}

	
	// Update is called once per frame
	void Update () {


		if (!triangleIsMoving) {
			Vector3 difference = target.transform.position - this.transform.position;

			float rotationInZ = Mathf.Atan2 (difference.y, difference.x) * Mathf.Rad2Deg;

			transform.rotation = Quaternion.Euler (new Vector3 (0f, 0f, rotationInZ));
		}
		if (Input.GetMouseButtonDown (0)) {
			Instantiate (bullet, turretTipLocation.position, turretTipLocation.rotation);
		}
		if (Input.GetMouseButtonDown (1)) {
			Debug.Log (triangleIsMoving);
			if (!triangleIsMoving) {
				Debug.Log ("start");
				StartCoroutine (moveTriangle ());
			}
		}



	}


}
