﻿using UnityEngine;
using System.Collections;

public class mouseControl : MonoBehaviour {


	GameObject breadCrumb;

	turretRotation triangleScript;

	// Use this for initialization
	void Start () {
		breadCrumb = Resources.Load ("Breadcrumb") as GameObject;
		triangleScript = GameObject.Find ("TurretRotator").GetComponent<turretRotation> ();
	}

	void MouseMove()
	{

		Vector3 mouseWorld = Camera.main.ScreenToWorldPoint (Input.mousePosition);


		this.transform.position = new Vector3 (mouseWorld.x,mouseWorld.y);
	}

	
	// Update is called once per frame
	void Update () {
		MouseMove ();

		//right click
		if (Input.GetMouseButtonDown (1)) {
			Instantiate (breadCrumb, transform.position, transform.rotation);
			triangleScript.trail.Add (transform.position);
		}
	}
}
