﻿using UnityEngine;
using System.Collections;

public class cubeController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}






	void goToFarClippingPlane()
	{
		transform.position = new Vector3 (0f, 0f, 50f);
	}


	void goToNearClippingPlane()
	{
		transform.position = new Vector3 (0f, 0f, 0.3f);
	}

	void rotateInX()
	{
		transform.Rotate (new Vector3 (1f, 0f, 0f) * 25f * Time.deltaTime);
	}

	void rotateInY()
	{
		transform.Rotate (new Vector3 (0f, 1f, 0f) * 25f * Time.deltaTime);
	}

	void rotateInZ()
	{
		transform.Rotate (new Vector3 (0f, 0f, 1f) * 25f * Time.deltaTime);
	}

	void moveInZ()
	{
		if (transform.position.z > 0.3f)
			transform.Translate (new Vector3 (0f, 0f, -1f) * 50f * Time.deltaTime);
		else
			goToFarClippingPlane ();
	}

	bool rotatex,rotatey,rotatez,movez = false;


	Vector3 bottomLeftOfViewPort()
	{
		//this corresponds to the bottom left of the screen at distance 10 from the camera
		return Camera.main.ViewportToWorldPoint(new Vector3(0f,0f,10f));
	}

	Vector3 bottomRightOfViewPort()
	{
		return Camera.main.ViewportToWorldPoint(new Vector3(1f,0f,10f));
	}


	Vector3 topLeftOfViewPort()
	{
		//this corresponds to the bottom left of the screen at distance 10 from the camera
		return Camera.main.ViewportToWorldPoint(new Vector3(0f,1f,10f));
	}

	Vector3 topRightOfViewPort()
	{
		//this corresponds to the bottom left of the screen at distance 10 from the camera
		return Camera.main.ViewportToWorldPoint(new Vector3(1f,1f,10f));
	}


	// Update is called once per frame
	void Update () {

		transform.Rotate (Vector3.up * 50f * Input.GetAxis ("Horizontal") * Time.deltaTime);
		transform.Translate (Vector3.forward * 10f * Input.GetAxis ("Vertical") * Time.deltaTime);


		//s and d for the other axes
		/*if (Input.GetKeyDown (KeyCode.A)) {
			rotatex = !rotatex;
		}

		if (Input.GetKeyDown (KeyCode.S)) {
			rotatey = !rotatey;
		}

		if (Input.GetKeyDown (KeyCode.D)) {
			rotatez = !rotatez;
		}

		if (Input.GetKeyDown (KeyCode.Space)) {
			movez = !movez;
		}


		if (Input.GetKeyDown (KeyCode.Z)) {
			transform.position = bottomLeftOfViewPort () + new Vector3 (0.5f, 0.5f, 0.5f);
		}

		if (Input.GetKeyDown (KeyCode.X)) {
			transform.position = bottomRightOfViewPort () + new Vector3 (-0.5f, 0.5f, 0.5f);
		}

		if (Input.GetKeyDown (KeyCode.Q)) {
			transform.position = topLeftOfViewPort () + new Vector3 (0.5f, -0.5f, 0.5f);
		}

		if (Input.GetKeyDown (KeyCode.W)) {
			transform.position = topRightOfViewPort () + new Vector3 (-0.5f, -0.5f, 0.5f);
		}



		if (movez)
			moveInZ ();


		if (rotatex)
			rotateInX ();

		if (rotatey)
			rotateInY ();

		if (rotatez)
			rotateInZ ();*/
	
	}
}
