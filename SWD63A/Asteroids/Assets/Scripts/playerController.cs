﻿using UnityEngine;
using System.Collections;

public class playerController : MonoBehaviour
{

	public float speed;

	public GameObject laser;

	float forwardimpulse;
	float backimpulse;

	generalController currentController;

	// Use this for initialization
	void Start ()
	{
		//place spaceship in the center of the screen
		transform.position = new Vector3 (0f, 0f);
		currentController = GameObject.FindGameObjectWithTag ("GameController").GetComponent<generalController> ();


	}

	public float limitX; 
	public float limitY;

	//keep on screen
	Vector3 clampPosition (Vector3 position)
	{
		return new Vector3 (
			Mathf.Clamp (position.x, -limitX, limitX),
			Mathf.Clamp (position.y, -limitY, limitY),
			0f);
	}

	//overflow off and on screen
	public Vector3 translatePosition (Vector3 position)
	{
		//Debug.Log (limitX);
		if ((position.x > limitX) || (position.x < -limitX))
			return new Vector3 (
				-position.x,
				position.y,
				0f);
		
		if ((position.y > limitY) || (position.y < -limitY))
			return new Vector3 (
				position.x,
				-position.y,
				0f);
		return position;
	}


	//set random position = hyperspace
	public Vector3 randomPosition(Vector3 position)
	{
		return translatePosition (new Vector3 (Random.Range (-limitX, limitX), Random.Range (-limitY, limitY), 0f));
	}






	void OnTriggerEnter2D (Collider2D otherObject)
	{
		if (otherObject.gameObject.tag == "alienbullet") {
			Destroy (otherObject.gameObject);
			currentController.livesLeft--;
			if (currentController.livesLeft == 0) {
				Time.timeScale=0f;
			}
		}
	}

	// Update is called once per frame
	void Update ()
	{
		

		limitX = Camera.main.orthographicSize * Camera.main.aspect;
		limitY = Camera.main.orthographicSize;

		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			forwardimpulse = 1f;
		}

		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			backimpulse = 1f;
		}

		if (Input.GetKeyDown (KeyCode.H)) {
			transform.position = randomPosition (transform.position);
		}

		if (Input.GetKeyDown (KeyCode.Space)) {
			Vector3 gun = GameObject.Find ("Gun").GetComponent<Transform> ().position;
			laser.tag = "bullet";
			Instantiate (laser, gun, transform.rotation);
		}


		if (forwardimpulse > 0f)
			forwardimpulse -= 0.03f;

		if (backimpulse > 0f)
			backimpulse -= 0.03f;
		
		if (forwardimpulse < 0f)
			forwardimpulse = 0f;
	
		if (backimpulse < 0f)
			backimpulse = 0f;

		transform.Translate (new Vector3 (0f, 1f) * speed * Time.deltaTime * forwardimpulse);
		transform.Translate (new Vector3 (0f, -1f) * speed * Time.deltaTime * backimpulse);
		//horizontal movement
		transform.Rotate (new Vector3 (0f, 0f, -1f) * speed * 10 * Time.deltaTime * Input.GetAxis ("Horizontal"));
		transform.position = translatePosition (transform.position);

	}
}
