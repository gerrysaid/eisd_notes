﻿using UnityEngine;
using System.Collections;

public class alienController : MonoBehaviour
{

	public float alienSpeed;
	public GameObject bullet;

	generalController currentController;
	playerController spaceshipController;

	// Use this for initialization
	void Start ()
	{
		StartCoroutine (changeDirection ());
		StartCoroutine (shoot ());
		currentController = GameObject.FindGameObjectWithTag ("GameController").GetComponent<generalController> ();
		spaceshipController = GameObject.FindGameObjectWithTag ("Player").GetComponent<playerController> ();
	}

	int damage = 0;

	IEnumerator shoot()
	{
		while (true) {
			bullet.tag = "alienbullet";
			Instantiate (bullet, transform.position, Quaternion.Euler (new Vector3 (0f, 0f, 180f)));
			yield return new WaitForSeconds (3f);
		}

	}


	void OnTriggerEnter2D (Collider2D otherObject)
	{
		if (otherObject.gameObject.tag == "bullet") {
			if (this.tag == "alien") {
				if (damage < 10) {
					damage++;
					currentController.score += 20;
					Destroy (otherObject.gameObject);
				} else {
					currentController.score += 30;
					Destroy (otherObject.gameObject);
					Destroy (this.gameObject);

				}
			}

		}
		//if the alien hits another asteroid
		if (otherObject.gameObject.tag == "asteroid") {
			Destroy (this.gameObject);
			Destroy (otherObject.gameObject);
		}

		if (otherObject.gameObject.tag == "Player") {
			currentController.livesLeft -= 1;
			damage++;
			//place the spaceship in a random position 
			spaceshipController.gameObject.transform.position = spaceshipController.randomPosition (spaceshipController.gameObject.transform.position);
		}





			
	}


	IEnumerator changeDirection()
	{
		while (true) {
			dirX = Mathf.Round(Random.Range (-1f, 2f));
			//dirY = Mathf.Round(Random.Range (-1f, 2f));
			yield return new WaitForSeconds (5f);
		}
	}

	float dirX;
	float dirY=0f;

	// Update is called once per frame
	void Update ()
	{
		transform.Rotate (new Vector3 (0f, 0f, 1f) * alienSpeed * Time.deltaTime);
		transform.Translate (new Vector3 (dirX, dirY) * Time.deltaTime,Space.World);
		transform.position = spaceshipController.translatePosition (transform.position);
	}
}
