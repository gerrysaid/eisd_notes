﻿using UnityEngine;
using System.Collections;

public class spaceshipController : MonoBehaviour {

	gridGenerator myGrid;


	public GameObject laserShot;

	// Use this for initialization
	void Start () {
		myGrid = GameObject.Find ("ParentObject").GetComponent<gridGenerator> ();
	
	}

	//clamp position to the bottom of the screen
	Vector3 clampPosition (Vector3 position)
	{
		return new Vector3 (
			Mathf.Clamp (position.x, -myGrid.limitX+(transform.localScale.x/2), myGrid.limitX-(transform.localScale.x/2)),
			-4.5f,
			0f);
	}

	// Update is called once per frame
	void Update () {

		transform.Translate(Vector3.right * 15f * Input.GetAxis("Horizontal")*Time.deltaTime);
		transform.position = clampPosition (transform.position);		

		//shooting
		if (Input.GetKeyDown (KeyCode.Space)) {
			Vector3 shootPosition = GameObject.Find ("Gun").transform.position;
			Instantiate (laserShot, shootPosition, Quaternion.identity);
		}
	}
}
