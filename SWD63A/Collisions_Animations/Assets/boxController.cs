﻿using UnityEngine;
using System.Collections;

public class boxController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		//up arrow pushing 
		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			this.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (0f, 10f, 0f), ForceMode2D.Impulse);
		}

		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			this.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (-10f, 0f, 0f), ForceMode2D.Impulse);
		}

		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			this.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (10f, 0f, 0f), ForceMode2D.Impulse);
		}

	
	}

	void OnCollisionEnter2D(Collision2D theCollision)
	{
		if (theCollision.collider.gameObject.tag == "target") {

			GameObject fallingBox = theCollision.collider.gameObject;
			fallingBox.GetComponent<SpriteRenderer> ().color = Color.red;

		}

		if (theCollision.collider.gameObject.tag == "floor") {
			GameObject floor = theCollision.collider.gameObject;
			floor.GetComponent<Animator> ().SetTrigger ("StartFloorAnimation");

		}

	}

	void OnCollisionExit2D(Collision2D theCollision)
	{
		if (theCollision.collider.gameObject.tag == "target") {

			GameObject fallingBox = theCollision.collider.gameObject;
			fallingBox.GetComponent<SpriteRenderer> ().color = Color.white;

		}
	}




}
