﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class mouseController : MonoBehaviour {
	public GameObject bullet;
	GameObject gun;
	Transform mPosition;

	GameObject wayPoint;

	GameObject positionText;

	Transform trianglePosition;

	playerController playerInstance;
	public bool isMoving = false;

	// Use this for initialization
	void Start () {
		//get the reference to the gun which is attached to the rotating triangle
		//load the waypoint prefab from the resources folder
		wayPoint = Resources.Load ("Prefabs/Waypoint") as GameObject;

		positionText = Resources.Load ("Prefabs/HighlightText") as GameObject;


		gun = GameObject.Find ("Gun");
		trianglePosition = GameObject.Find ("TriangleParent").GetComponent<Transform> ();
		playerInstance = GameObject.Find ("TriangleParent").GetComponent<playerController> ();

	}


	void mouseMove(){
		//this gives me the position of the mouse
		Vector3 mousePosition = Input.mousePosition;

		GameObject.Find ("DebugText").GetComponent<Text> ().text = mousePosition.ToString();

		Vector3 squarePosition = Camera.main.ScreenToWorldPoint (mousePosition);

		GameObject.Find ("DebugText").GetComponent<Text> ().text += "\n"+squarePosition.ToString();
		GameObject.Find ("DebugText3").GetComponent<Text> ().text = mousePosition.ToString();
		GameObject.Find ("DebugText3").GetComponent<Text> ().text += "\n"+squarePosition.ToString();

		transform.position = new Vector3 (squarePosition.x, squarePosition.y);

	}



	IEnumerator moveTriangle()
	{
		int i = 0;
		while(i<playerInstance.positionsToTraverse.Count){	
			
			while (Vector3.Distance (trianglePosition.position, playerInstance.positionsToTraverse[i]) > 0.5f) {
				isMoving = true;
				//last parameter is the step (by how much is it moving)

				Vector3 rotationalDifference = playerInstance.positionsToTraverse[i] - trianglePosition.position;

				//this is to get the difference in the rotations so it points towards the mouse cursor
				float rotationInZ = Mathf.Atan2 (rotationalDifference.y, rotationalDifference.x) * Mathf.Rad2Deg;

				trianglePosition.rotation = Quaternion.Euler (0f, 0f, rotationInZ);

				trianglePosition.position = Vector3.MoveTowards (trianglePosition.position, playerInstance.positionsToTraverse [i], 1f);
				yield return new WaitForSeconds (0.2f);
			}

			yield return null;
			i++;
		}
		isMoving = false;
		yield return null;

	}

	int counter = 0;

	// Update is called once per frame
	void Update () {
		//Cursor.visible = false;
		mouseMove ();
		//left click
		if (Input.GetMouseButtonDown (0)) {
			//shoot
			GameObject tempBullet = Instantiate(bullet,gun.transform.position,gun.transform.rotation) as GameObject;
			//push it with a force of 10 forward.  It will keep going until it stops
			tempBullet.GetComponent<Rigidbody2D> ().AddRelativeForce (new Vector3 (0f, 10f),ForceMode2D.Impulse);

		}

		Transform trianglePosition = GameObject.Find ("TriangleParent").transform;



		//RIGHT CLICK
		if (Input.GetMouseButtonDown (1)) {


		


			//position of the mouse right click

			//this is adding the exact current position of the square at the moment
			playerInstance.positionsToTraverse.Add(transform.position);
			//spawn the box
			GameObject temp = Instantiate (wayPoint, transform.position, Quaternion.identity) as GameObject;

			GameObject temp2 = Instantiate (positionText, transform.position, Quaternion.identity) as GameObject;

			temp2.transform.SetParent(temp.transform);
			counter++;
			temp2.GetComponentInChildren<Text> ().text = counter.ToString ();

			if (!isMoving)
				StartCoroutine (moveTriangle ());

		}

	
	}
}
