﻿using UnityEngine;
//to manipulate text
using UnityEngine.UI;
//
using System.Collections;

public class penguinScript : MonoBehaviour {

	public float penguinSpeed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.Space)) {
			penguinSpeed = 0f;
		}

		//to implement: Press another key to start it rotating again


		//to implement: Press another (different) key to rotate it in the opposite direction


		transform.Rotate (Vector3.forward * penguinSpeed * Time.deltaTime);

		//get the angle text and change its property
		GameObject.Find("AngleText").GetComponent<Text>().text = "Angle: "+transform.rotation.z;
	}
}
