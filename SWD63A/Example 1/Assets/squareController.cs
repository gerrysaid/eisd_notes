﻿using UnityEngine;

using UnityEngine.UI;

using System.Collections;

public class squareController : MonoBehaviour
{
	public float speed;

	public float countDown;

	public bool mode;

	bool gameStarted = false;


	float positionX;
	float positionY;

	Vector3 screenEdge;


	public GameObject obstaclePrefab;


	void generateEnemy ()
	{
		float randomY = Random.Range (-Camera.main.orthographicSize, Camera.main.orthographicSize);
		float randomX = Random.Range (-Camera.main.orthographicSize * Camera.main.aspect, Camera.main.orthographicSize * Camera.main.aspect);


		Instantiate (obstaclePrefab, new Vector3 (randomX, randomY), Quaternion.identity);

	}

	//collision function
	void OnTriggerEnter2D (Collider2D otherObject)
	{
		//remove the obstacle
		Destroy (otherObject.gameObject);
		generateEnemy ();
	}


	// Use this for initialization
	void Start ()
	{
		StartCoroutine (countDownTimer ());
	
	}


	IEnumerator randomEnemyGenerator ()
	{
		while (true) {
			float randomGap = Random.Range (3f, 10f);
			generateEnemy ();
			yield return new WaitForSeconds (randomGap);
		}


	}

	IEnumerator countDownTimer ()
	{

		while (countDown > 0f) {
			GameObject.Find ("MiddleText").GetComponent<Text> ().text = countDown.ToString ();
			countDown--;
			yield return new WaitForSeconds (1f);
		}
		generateEnemy ();
		gameStarted = true;
		StartCoroutine (randomEnemyGenerator ());
	}
	
	// Update is called once per frame
	void Update ()
	{
		Debug.Log (gameStarted);

		if (gameStarted) {
			float minValue = 0f;
			float maxValue = 0f;

			//minimum and maximum values for Y axis
			minValue = -Camera.main.orthographicSize + (transform.localScale.y / 2f);
			maxValue = Camera.main.orthographicSize - (transform.localScale.y / 2f);

			//calculate the aspect ratio
			float aspectRatio = 0f;
			//	aspectRatio = (float)Screen.width / (float)Screen.height;

			aspectRatio = Camera.main.aspect;

			float minValueX = 0f;
			float maxValueX = 0f;

			minValueX = -Camera.main.orthographicSize * aspectRatio + (transform.localScale.y / 2f);
			maxValueX = Camera.main.orthographicSize * aspectRatio - (transform.localScale.y / 2f);


			Debug.Log ("X border min:" + minValueX);
			Debug.Log ("X border max:" + maxValueX);

			//clamp the X and Y axis
			transform.position = new Vector3 (Mathf.Clamp (transform.position.x, minValueX, maxValueX)
			, Mathf.Clamp (transform.position.y, minValue, maxValue));


			if (!mode)
				transform.Translate (new Vector3 (1f, 0f) * speed * Input.GetAxis ("Horizontal") * Time.deltaTime);
			else
				transform.Rotate (new Vector3(0f,0f,1f) * speed * 4 * Input.GetAxis ("Horizontal") * Time.deltaTime);

			transform.Translate (new Vector3 (0f, 1f) * speed * Input.GetAxis ("Vertical") * Time.deltaTime);


	


			positionX = transform.position.x;
			positionY = transform.position.y;

			//show position in world space
			GameObject.Find ("SystemText").GetComponent<Text> ().text = "X: " + positionX + " Y: " + positionY;
			//show width and height in pixels ie SCREEN space
			GameObject.Find ("SystemText").GetComponent<Text> ().text += "\nW: " + Screen.width + " H: " + Screen.height;

			//convert screen edge to world coordinates
			screenEdge = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, Screen.height));
			//show width and height in pixels ie WORLD space
			GameObject.Find ("SystemText").GetComponent<Text> ().text += "\nW: " + screenEdge.x + " H: " + screenEdge.y;
		}

	}
}
