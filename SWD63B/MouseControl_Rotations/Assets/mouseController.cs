﻿using UnityEngine;
using System.Collections;

public class mouseController : MonoBehaviour {
	public GameObject bullet;

	GameObject bulletSource;

	// Use this for initialization
	void Start () {
		bulletSource = GameObject.Find ("Tip");
	}


	void mouseMovement()
	{

		Vector3 mousePosition = Input.mousePosition;

		Vector3 boxPosition = Camera.main.ScreenToWorldPoint (mousePosition);

		transform.position = new Vector3 (boxPosition.x, boxPosition.y);


	}

	bool move=true;

	// Update is called once per frame
	void Update () {
		if (move)
			mouseMovement ();

		if (Input.GetMouseButtonDown (0)) {
			move = !move;
		}

		if (Input.GetMouseButtonDown (1)) {

			Instantiate (bullet, bulletSource.transform.position, bulletSource.transform.rotation);
		}

	}
}
