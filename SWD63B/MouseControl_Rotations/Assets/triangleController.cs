﻿using UnityEngine;
using System.Collections;

public class triangleController : MonoBehaviour {
	GameObject target;


	// Use this for initialization
	void Start () {
		target = GameObject.Find ("Target");
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 difference =  target.transform.position - this.transform.position;

		float zRotation = Mathf.Atan2 (difference.y, difference.x) * Mathf.Rad2Deg;

		transform.rotation = Quaternion.Euler (0f, 0f, zRotation);


	
	}
}
