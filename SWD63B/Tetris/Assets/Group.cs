﻿using UnityEngine;
using System.Collections;

public class Group : MonoBehaviour {

    void Start()
    {
        // Default position not valid? Then it's game over
        if (!isValidGridPos())
        {
            Debug.Log("GAME OVER");
            Destroy(gameObject);
        }
    }

    public bool isValidGridPos()
    {
        foreach (Transform child in transform)
        {
            Vector3 v = Grid.roundVec(child.position);

            // Not inside Border?
            Debug.Log(v);
            if (!Grid.insideBorder(v))
                return false;

            // Block in grid cell (and not part of same group)?
            if (Grid.grid[(int)v.x, (int)v.y] != null &&
                Grid.grid[(int)v.x, (int)v.y].parent != transform)
                return false;
        }
        return true;
    }

    public void updateGrid()
    {
        // Remove old children from grid
        for (int y = 0; y < Grid.h; ++y)
            for (int x = 0; x < Grid.w; ++x)
                if (Grid.grid[x, y] != null)
                    if (Grid.grid[x, y].parent == transform)
                        Grid.grid[x, y] = null;

        // Add new children to grid
        foreach (Transform child in transform)
        {
            Vector2 v = Grid.roundVec(child.position);
            Debug.Log("b"+v);
            Grid.grid[(int)v.x, (int)v.y] = child;
        }
    }

   
}
