﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class spawner : MonoBehaviour {

    public List<GameObject> allBlocks;
    GameObject currentBlock;

    Group groupScript;

    float time = 0f;

	// Use this for initialization
	void Start () {
        time = Time.time;
        allBlocks.AddRange(Resources.LoadAll<GameObject>("Tetrisblock"));
        spawnBlock();
	}


    void spawnBlock()
    {
        currentBlock = Instantiate(allBlocks[Random.Range(0, allBlocks.Count)], transform.position, Quaternion.identity) as GameObject;
        groupScript = currentBlock.GetComponent<Group>();

    }

	
	// Update is called once per frame
	void Update () {
        if ((Time.time - time > 1f) || (Input.GetKeyDown(KeyCode.DownArrow)))
        {
            time = Time.time;
            currentBlock.transform.position -= new Vector3(0f, 1f);
            if (groupScript.isValidGridPos())
            {
                // Its valid. Update grid.
                groupScript.updateGrid();

            }
            else
            {
                // It's not valid. revert.
                currentBlock.transform.position += new Vector3(0, 1, 0);

                // Clear filled horizontal lines
                Grid.deleteFullRows();


                spawnBlock();
            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            currentBlock.transform.rotation *= Quaternion.Euler(0f, 0f, -90f);
            if (groupScript.isValidGridPos())
                // Its valid. Update grid.
                groupScript.updateGrid();
            else
                currentBlock.transform.rotation *= Quaternion.Euler(0f, 0f, 90f);
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            currentBlock.transform.position -= new Vector3(1f, 0f);

            if (groupScript.isValidGridPos())
                // Its valid. Update grid.
                groupScript.updateGrid();
            else
                // Its not valid. revert.
                currentBlock.transform.position += new Vector3(1f, 0, 0);
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            currentBlock.transform.position += new Vector3(1f, 0f);

            if (groupScript.isValidGridPos())
                // Its valid. Update grid.
                groupScript.updateGrid();
            else
                // Its not valid. revert.
                currentBlock.transform.position -= new Vector3(1f, 0, 0);
        }

    

    }
}
