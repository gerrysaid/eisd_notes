﻿using UnityEngine;
using System.Collections;

public class Grid : MonoBehaviour {

    /// <summary>
    /// The coordinates as explained in the grid are catered for by placing the camera at position in
    /// x 4.5 and y 7.5.  This means that in practice, if we place any of the blocks in position 0,0, the bottom
    /// left of the block is touching the edge of the play area, which is a 10 block wide area in the actual
    /// physical middle of the screen.  Therefore, we can subdivide the whole area of the screen in a fixed area 
    /// of 10 by 20, without having to scale individual elements.  The legal coordinates for a group of blocks is
    /// between 0 and 10, given that any of the child blocks are outside the play area. 
    /// </summary>


    public static int w = 10;
    public static int h = 20;
    public static Transform[,] grid = new Transform[w, h];


    //ensure that rotations are catered for
    public static Vector3 roundVec(Vector3 v)
    {
        return new Vector3(Mathf.Round(v.x),
                           Mathf.Round(v.y));
    }

    //check if the parent block is inside the border
    public static bool insideBorder(Vector3 pos)
    {
        return ((int)pos.x >= 0 &&
                (int)pos.x < w &&
                (int)pos.y >= 0);
    }

    //delete any objects at a specific row
    public static void deleteRow(int y)
    {
        for (int x = 0; x < w; ++x)
        {
            Destroy(grid[x, y].gameObject);
            grid[x, y] = null;
        }
    }

    //move the row above the specific row one row down and update all positions
    public static void decreaseRow(int y)
    {
        for (int x = 0; x < w; ++x)
        {
            if (grid[x, y] != null)
            {
                // Move one towards bottom
                grid[x, y - 1] = grid[x, y];
                grid[x, y] = null;

                // Update Block position
                grid[x, y - 1].position += new Vector3(0, -1, 0);
            }
        }
    }

    //do this for all the rows above a certain row
    public static void decreaseRowsAbove(int y)
    {
        for (int i = y; i < h; ++i)
            decreaseRow(i);
    }

    //check if there is a line
    public static bool isRowFull(int y)
    {
        for (int x = 0; x < w; ++x)
            if (grid[x, y] == null)
                return false;
        return true;
    }

    //combine everything (we can do scoring here)
    public static void deleteFullRows()
    {
        for (int y = 0; y < h; ++y)
        {
            if (isRowFull(y))
            {
                deleteRow(y);
                decreaseRowsAbove(y + 1);
                --y;
            }
        }
    }


}
