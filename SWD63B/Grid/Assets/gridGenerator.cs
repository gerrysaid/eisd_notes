﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class gridGenerator : MonoBehaviour {

	//rows and columns of the grid
	public int rows,columns = 0;

	//the objects we are going to fill the grid with
	public GameObject redSquare,whiteSquare;


	public List<GameObject> topRow,bottomRow,firstColumn,lastColumn;

	List<GameObject> allBoxes = new List<GameObject>();

	bool top,last,bottom = false;


	IEnumerator flipColor(GameObject box)
	{
		Color prevColor = box.GetComponent<SpriteRenderer> ().color;
		box.GetComponent<SpriteRenderer> ().color = Color.green;
		yield return new WaitForSeconds (0.5f);
		box.GetComponent<SpriteRenderer> ().color = prevColor;


	}


	IEnumerator animateBox()
	{

		while (true) {
			
			if (top) {
				foreach (GameObject box in lastColumn.Skip(1)) {
					//this means wait for the coroutine to return the value
					yield return StartCoroutine (flipColor (box));
					last = true;
				}
			} 
			if (top && last) {
				foreach (GameObject box in bottomRow.Skip(1)) {
					yield return StartCoroutine (flipColor (box));
					bottom = true;
				}
			} 
			if (top && last && bottom) {
				foreach (GameObject box in firstColumn.Skip(1)) {
					yield return StartCoroutine (flipColor (box));
				}
				top=last=bottom=false;
			}
			if (!top && !last && !bottom) {
				foreach (GameObject box in topRow.Skip(1)) {
					yield return StartCoroutine (flipColor (box));
					top = true;
				}

				//
			}
		}
				


	}



	public void generateGrid(){
		GameObject tempObject;

		for (int r = 0; r < rows; r++) {
			for (int c=0;c < columns; c++){

				//implement chessboard
				if ((r + c) % 2 == 0) 
					tempObject = Instantiate (whiteSquare, new Vector3 (c, r), Quaternion.identity) as GameObject;
				 else 
					tempObject = Instantiate (redSquare, new Vector3 (c, r), Quaternion.identity) as GameObject;
				
				tempObject.GetComponent<boxScript> ().row = r;
				tempObject.GetComponent<boxScript> ().column = c;
				tempObject.transform.parent = this.transform;
				//a unique name for each element
				tempObject.name = "Row: " + r + " Column:" + c;
				allBoxes.Add (tempObject);
				}
			}
		}



	float limitX,limitY=0f;
	float scalefactorX,scalefactorY = 0f;

	public void scaleGrid()
	{
		limitX = Camera.main.orthographicSize * Camera.main.aspect;
		limitY = Camera.main.orthographicSize;
		transform.position = new Vector3 (-limitX, 0);
		//scale the grid to fit on screen



		//the scale for each box
		scalefactorY = (limitY)/rows;
		scalefactorX = (limitX*2)/columns;

		//after you finish the scaling, alternate between white and red square (if statement in the for loop)
		transform.localScale = new Vector3 (scalefactorX, scalefactorY);



	}


	// Use this for initialization
	void Start () {
		generateGrid ();
		scaleGrid ();

		topRow = allBoxes.FindAll (topRowBox => topRowBox.GetComponent<boxScript> ().row == 0);
		bottomRow = allBoxes.FindAll (topRowBox => topRowBox.GetComponent<boxScript> ().row == rows-1);

		firstColumn = allBoxes.FindAll (topRowBox => topRowBox.GetComponent<boxScript> ().column == 0);
		lastColumn = allBoxes.FindAll (topRowBox => topRowBox.GetComponent<boxScript> ().column == columns-1);

		//to keep the ordering
		bottomRow.Reverse ();
		firstColumn.Reverse ();

		StartCoroutine (animateBox ());

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
