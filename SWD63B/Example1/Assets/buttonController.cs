﻿using UnityEngine;
//to be able to switch scenes
using UnityEngine.SceneManagement;
using System.Collections;

public class buttonController : MonoBehaviour {


	public void newButtonPressed()
	{
		SceneManager.LoadScene ("penguinscene");
		Debug.Log ("penguin scene");
	}

	public void quitButtonPressed()
	{
		SceneManager.LoadScene ("quitscene");
		Debug.Log ("quit scene");
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
