﻿using UnityEngine;

using UnityEngine.UI;

using System.Collections;

public class squareController : MonoBehaviour
{
	public float speed;

	public GameObject obstacle;

	bool canPlay = false;


	int enemyCounter = 0;

	IEnumerator countDown ()
	{
		int cdown = 5;
		while (cdown > 0) {
			cdown--;
			GameObject.Find ("CountdownText").GetComponent<Text> ().text = cdown.ToString ();
			yield return new WaitForSeconds (1f);
		}
		Destroy (GameObject.Find ("CountdownText"));
		StartCoroutine(enemyGenerator ());
		canPlay = true;
	}

	IEnumerator enemyGenerator ()
	{
		while (enemyCounter<60) {
			generateEnemy ();
			yield return new WaitForSeconds (10f);
		}
			
	}



	// Use this for initialization
	void Start ()
	{

		//as soon as the game starts, create a new obstacle at that position
		StartCoroutine (countDown ());
	
	}


	void generateEnemy ()
	{
		float randomX;
		float randomY;

		Vector3 enemyPosition;
		do {
			//the y edges are based on the camera.main.orthographicsize
			randomY = Random.Range (-Camera.main.orthographicSize, Camera.main.orthographicSize);

			//the x edges are based on the camera.main.orthographicsize * the aspect ratio
			randomX = Random.Range (-Camera.main.orthographicSize * Camera.main.aspect, Camera.main.orthographicSize * Camera.main.aspect);


			enemyPosition = new Vector3 (randomX, randomY);
			//create the enemy at a random position
		} while (Vector3.Distance (transform.position, enemyPosition) < transform.localScale.magnitude);

		Instantiate (obstacle, new Vector3 (randomX, randomY), Quaternion.identity);
		enemyCounter++;
	}



	//collision method of box to obstacle
	void OnTriggerEnter2D (Collider2D otherCollider)
	{
		//using the reference to the other object
		Debug.Log (otherCollider.gameObject.name);

		GetComponent<SpriteRenderer> ().color = Color.red;
		//enemy to be destroyed
		Destroy (otherCollider.gameObject);

		//generate a new enemy at a random position
		generateEnemy ();

	}

	void OnTriggerExit2D (Collider2D otherCollider)
	{
		GetComponent<SpriteRenderer> ().color = Color.white;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (canPlay) {
			transform.Translate (new Vector3 (1f, 0f) * speed * Input.GetAxis ("Horizontal") * Time.deltaTime);
			transform.Translate (new Vector3 (0f, 1f) * speed * Input.GetAxis ("Vertical") * Time.deltaTime);


			float xPos;
			float yPos;

			xPos = transform.position.x;
			yPos = transform.position.y;
			//the position of the square relative to the center of the screen
			GameObject.Find ("DebugText").GetComponent<Text> ().text = "X: " + xPos + " Y: " + yPos;
			//the size in pixels of the game window
			GameObject.Find ("DebugText").GetComponent<Text> ().text += "\nX: " + Screen.width + " Y: " + Screen.height;
			//the position of the square in pixels relative to the bottom left of the screen
			Vector3 screenPos = Camera.main.WorldToScreenPoint (transform.position);
			GameObject.Find ("DebugText").GetComponent<Text> ().text += "\nX: " + screenPos.x + " Y: " + screenPos.y;

			float aspectRatio; 

			aspectRatio = (float)Screen.width / (float)Screen.height;


			if (xPos > Camera.main.orthographicSize * aspectRatio) {
				transform.position = new Vector3 (-Camera.main.orthographicSize * aspectRatio, yPos);
			}


			if (xPos < -Camera.main.orthographicSize * aspectRatio) {
				transform.position = new Vector3 (Camera.main.orthographicSize * aspectRatio, yPos);
			}


			if (yPos > Camera.main.orthographicSize) {
				transform.position = new Vector3 (xPos, -Camera.main.orthographicSize);
			}


			if (yPos < -Camera.main.orthographicSize) {
				transform.position = new Vector3 (xPos, Camera.main.orthographicSize);
			}
		}
	}
}
