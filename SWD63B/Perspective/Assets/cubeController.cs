﻿using UnityEngine;
using System.Collections;

public class cubeController : MonoBehaviour {


	bool rotatex,rotatey,rotatez=false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.A))
			rotatex = !rotatex;

		if (Input.GetKeyDown (KeyCode.S))
			rotatey = !rotatey;
		
		if (Input.GetKeyDown (KeyCode.D))
			rotatez = !rotatez;

		if (rotatex)
			transform.Rotate (new Vector3 (1f, 0f,0f) * 25f * Time.deltaTime);

		if (rotatey)
			transform.Rotate (new Vector3 (0f, 1f,0f) * 25f * Time.deltaTime);

		if (rotatez)
			transform.Rotate (new Vector3 (0f, 0f,1f) * 25f * Time.deltaTime);

		//bottom left corner
		if (Input.GetKeyDown(KeyCode.Z)){
			Vector3 bottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(0f,0f,10f));
			transform.position = new Vector3 (bottomLeft.x, bottomLeft.y, transform.position.z) + new Vector3 (0.5f, -0.5f);
		}


		//bottom right corner
		if (Input.GetKeyDown(KeyCode.X)){
			Vector3 bottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(1f,0f,10f));
			transform.position = new Vector3 (bottomLeft.x, bottomLeft.y, transform.position.z) + new Vector3 (-0.5f, 0.5f);
		}



		//top right corner
		if (Input.GetKeyDown(KeyCode.W)){
			Vector3 bottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(1f,1f,10f));
			transform.position = new Vector3 (bottomLeft.x, bottomLeft.y, transform.position.z) + new Vector3 (0.5f, -0.5f);
		}


		//top left corner
		if (Input.GetKeyDown(KeyCode.Q)){
			Vector3 bottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(0f,1f,10f));
			transform.position = new Vector3 (bottomLeft.x, bottomLeft.y, transform.position.z) + new Vector3 (0.5f, -0.5f);
		}


	}
}
