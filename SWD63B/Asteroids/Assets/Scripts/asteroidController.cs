﻿using UnityEngine;
using System.Collections;

public class asteroidController : MonoBehaviour
{

	public float asteroidSpeed;
	public GameObject smallAsteroid;

	generalController currentController;
	playerController spaceshipController;

	// Use this for initialization
	void Start ()
	{
		StartCoroutine (changeDirection ());
		currentController = GameObject.FindGameObjectWithTag ("GameController").GetComponent<generalController> ();
		spaceshipController = GameObject.FindGameObjectWithTag ("Player").GetComponent<playerController> ();
	}




	void OnTriggerEnter2D (Collider2D otherObject)
	{
		if (otherObject.gameObject.tag == "bullet") {
			if (this.tag == "asteroid") {
				currentController.score += 30;
				Destroy (otherObject.gameObject);
				for(int i=0;i<Random.Range(1,5);i++)
					Instantiate (smallAsteroid, transform.position, transform.rotation);
				Destroy (this.gameObject);
			}
			if (this.tag == "small_asteroid") {
				currentController.score += 100;
				Destroy (otherObject.gameObject);
				Destroy (this.gameObject);
			}
		}
		//if one asteroid hits another asteroid
		if (otherObject.gameObject.tag == "asteroid") {
			Destroy (this.gameObject);
			Destroy (otherObject.gameObject);
		}

		if (otherObject.gameObject.tag == "Player") {
			currentController.livesLeft -= 1;
			Destroy (this.gameObject);
		}





			
	}


	IEnumerator changeDirection()
	{
		while (true) {
			dirX = Mathf.Round(Random.Range (-1f, 2f));
			dirY = Mathf.Round(Random.Range (-1f, 2f));
			yield return new WaitForSeconds (5f);
		}
	}

	float dirX;
	float dirY;

	// Update is called once per frame
	void Update ()
	{
		transform.Rotate (new Vector3 (0f, 0f, 1f) * asteroidSpeed * Time.deltaTime,Space.Self);
		transform.Translate (new Vector3 (dirX, dirY) * Time.deltaTime,Space.World);
		transform.position = spaceshipController.translatePosition (transform.position);
	}
}
