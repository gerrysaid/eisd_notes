﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class generalController : MonoBehaviour {


	public int score;
	public int livesLeft;
	public int round;
	public int totalAsteroidsInRound;
	public int totalAliensInRound;



	public GameObject asteroid;
	public GameObject alien;


	float marginx;
	float marginy;


	bool checkPosition(Vector3 sourceposition,float range)
	{
		Collider2D[] results = new Collider2D[1];
		if (Physics2D.OverlapCircleNonAlloc(sourceposition,range,results)>0){
			return false;
		}
		return true;
	}


	IEnumerator spawnAsteroids()
	{
		int count = 0;
		while (count < totalAsteroidsInRound) {
			count++;
			Vector3 tempAsteroidPos;


			//keep generating a new asteroid position until the objects are not overlapping
			do{
				tempAsteroidPos = new Vector3 (
					Random.Range (-marginx + 0.5f, marginx - 0.5f),
					Random.Range (-marginy + 0.5f, marginy - 0.5f));
				yield return null;
			} while(!checkPosition(tempAsteroidPos,1f)); 



			Instantiate (asteroid, tempAsteroidPos,
				Quaternion.identity);
			
			yield return new WaitForSeconds(Random.Range(2f,6f));
				
		}
		yield return null;

	}


	IEnumerator spawnAliens()
	{
		int count = 0;
		while (count < totalAliensInRound) {
			count++;
			Vector3 tempAlienPos;


			//keep generating a new asteroid position until the objects are not overlapping
			do{
				tempAlienPos = new Vector3 (
					Random.Range (-marginx + 0.5f, marginx - 0.5f),
					Random.Range (0f, marginy - 0.5f));
				yield return null;
			} while(!checkPosition(tempAlienPos,1f)); 



			Instantiate (alien, tempAlienPos,
				Quaternion.identity);

			yield return new WaitForSeconds(Random.Range(5f,10f));

		}
		yield return null;

	}

			



	// Use this for initialization
	void Start () {
		
		StartCoroutine (spawnAsteroids ());
		StartCoroutine (spawnAliens ());
		round = 1;
		score = 0;
		if (livesLeft == 0)
			livesLeft = 3;
		
	}



	// Update is called once per frame
	void Update () {
		
		marginx = GameObject.FindGameObjectWithTag ("Player").GetComponent<playerController> ().limitX;
		marginy = GameObject.FindGameObjectWithTag ("Player").GetComponent<playerController> ().limitY;


		GameObject.Find ("Scoring").GetComponent<Text> ().text = 
			"Lives: " + livesLeft +
		"\nScore: " + score +
		"\nRound: " + round;

		//game over
		if (livesLeft == 0) {
			Time.timeScale = 0f;
			livesLeft = -1;
		}

	}
}
