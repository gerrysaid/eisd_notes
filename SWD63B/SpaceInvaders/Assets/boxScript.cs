﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class boxScript : MonoBehaviour {
	public int row,column;

	gridGenerator myGrid;

	void Start()
	{
		//every box knows about the grid, therefore knows about the aliens next to it
		myGrid = GameObject.Find ("ParentObject").GetComponent<gridGenerator> ();
	}






	bool checkEdgeColumn(int columnIndex)
	{
		List<GameObject> currentColumn = myGrid.allBoxes.FindAll (currentColumnBox => currentColumnBox.GetComponent<boxScript> ().column == column);
		if (currentColumn.Count == 0)
			return true;
		else
			return false;
	}

	//an alien gets killed here
	void OnTriggerEnter2D(Collider2D otherObject)
	{
		if (otherObject.tag == "bullet") {

			//how are we going to calculate the total width of the grid here? 
			//how are we going to trigger movement over here if we have space to move? 
			Debug.Log(row+" "+column);

			//how many are left in that particular column
			//remove the box I hit
			myGrid.allBoxes.Remove (this.gameObject);
			//how many boxes are left in the current column?
			//how do I know it's an edge column?
			//let us start from column 0


			if (this.column == myGrid.startingColumn) {
				//check the next column and break if there is at least one alien
				if (checkEdgeColumn (this.column)) {
					//can move grid one box to the right

					myGrid.transform.position -= new Vector3(myGrid.transform.localScale.x,0f,0f);

					//we neet to update starting column here
					myGrid.startingColumn++;

				}
			} else if (this.column == myGrid.endingColumn) {
				if (checkEdgeColumn (this.column)) {
					//can move grid one box to the left
					myGrid.transform.position += new Vector3(myGrid.transform.localScale.x,0f,0f);
					//we need to update ending column here
					myGrid.endingColumn--;

				}
			}
			Destroy (otherObject.gameObject);
			Destroy (this.gameObject);
			}
			
		}



}
